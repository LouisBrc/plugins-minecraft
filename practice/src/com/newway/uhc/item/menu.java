package com.newway.uhc.item;

import com.newway.uhc.commands.*;
import com.newway.uhc.kits.builduhc;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.newway.uhc.partylist.*;

import java.util.ArrayList;

public class menu implements Listener {

    public playerlist playerlist = new playerlist();

    public builduhc b = new builduhc();

    @EventHandler
    public void onClick(InventoryClickEvent event){

        Inventory inv = event.getInventory();
        Player player = (Player) event.getWhoClicked();
        ItemStack current = event.getCurrentItem();

        if(current == null) {
            return;
        }
        if(inv.getName().equalsIgnoreCase("§8Menu")){


            event.setCancelled(true);
            if(current.getType() == Material.DIAMOND_SWORD && current.hasItemMeta()){

                Location spawn = new Location(player.getWorld(), 0 , 96, 0);
                player.teleport(spawn);
                player.closeInventory();
            }
        }
        if (inv.getName().equalsIgnoreCase("§8MenuFFA") || inv.getName().equalsIgnoreCase("§8MenuTeam")){
            int place1 = place(player)[0];
            String mdj;
            if(inv.getName().equalsIgnoreCase("§8MenuFFA")){
                mdj = "FFA";
            }else{
                mdj = "Team";
            }
            event.setCancelled(true);
            if(current.getType() == Material.GOLDEN_APPLE && current.hasItemMeta()){
                if(isin(player)){
                    if (mdj == "FFA" || mdj == "Team"){
                        int i = place(player)[0];
                        int j = 0;
                        while(j < playerlist.playerlists.get(i).size()){
                            Location spawn = new Location(player.getWorld(), 0 , 96, 0);
                            Player partyplayers = (Player) playerlist.playerlists.get(i).get(j);
                            partyplayers.teleport(spawn);
                            j++;
                        }
                    }else{
                        int places = place(player)[0];
                        int j = 0;
                        ArrayList<Player> team1 = new ArrayList<Player>();
                        ArrayList<Player> team2 = new ArrayList<Player>();
                        ArrayList<Player> total = playerlist.playerlists.get(places);

                        while(total.size() > j){

                            team1.add(total.get(j));
                            j++;
                            team2.add(total.get(j));
                            j++;

                        }
                        Player ee = (Player) team2.get(0);
                        int j2 = 0;
                        int tot = 0;
                        while(tot < total.size()){
                            Location pvp1 = new Location(player.getWorld(), 0 , 96, 0);
                            Location pvp2 = new Location(player.getWorld(), 50 , 96, 0);
                            Player partyplayers1 = (Player) team1.get(j2);
                            partyplayers1.teleport(pvp1);
                            tot++;
                            Player partyplayers2 = (Player) team2.get(j2);
                            partyplayers2.teleport(pvp2);
                            tot++;
                            j2++;

                        }
                    }

                    player.closeInventory();
                }
                addinInventory(place1, "bu");
            }
        }
    }

    public void addinInventory(int place, String kit){
        int i = 0;
        while (playerlist.playerlists.get(place).size() > i){
            if(kit == "bu"){
                Player player = (Player) playerlist.playerlists.get(place).get(i);
                b.addbu(player);
            }
            i++;
        }

    }

    public boolean isin(Player player){
        int i = 0;
        boolean playerisin = false;
        while(playerlist.playerlists.size() > i){
            int j = 0;
            while(playerlist.playerlists.get(i).size()>j){
                if(playerlist.playerlists.get(i).get(j) == player){
                    playerisin = true;
                }
                j++;
            }
            i++;
        }
        return playerisin;
    }

    public int[] place(Player player){
        int[] tab = new int[2];
        int i = 0;
        boolean playerisin = false;
        while(playerlist.playerlists.size() > i && playerisin == false){
            int j = 0;
            while(playerlist.playerlists.get(i).size()>j && playerisin == false){
                if(playerlist.playerlists.get(i).get(j) == player){
                    playerisin = true;
                    tab[0] = i;
                    tab[1] = j;
                    System.out.println(j);
                }
                j++;
            }
            i++;

        }
        return tab;
    }

    public boolean isLeader(Player player){
        int i = 0;
        boolean leader = false;
        while(playerlist.playerlists.size() > i){
            if(playerlist.playerlists.get(i).get(0).equals(player)){
                leader = true;

            }
            i++;
        }
        return leader;
    }



    public void telepor(Player player) {
        int i = 0;
        int pla = place(player)[0];

        while (playerlist.playerlists.get(pla).size() > i) {
            Location pvp = new Location(player.getWorld(), 0 , 96, 0);
            Player a = playerlist.playerlists.get(pla).get(i);
            a.teleport(pvp);
            i++;
        }
    }

    public void sendAll(Player player, String message){
        int i = 0;
        int pla = place(player)[0];

        while (playerlist.playerlists.get(pla).size() > i) {
            Player a = playerlist.playerlists.get(pla).get(i);
            a.sendMessage(message);
            i++;
        }

    }
}
