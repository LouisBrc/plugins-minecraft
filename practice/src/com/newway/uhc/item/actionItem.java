

package com.newway.uhc.item;
import com.newway.uhc.commands.*;


import org.bukkit.entity.Player;
import org.bukkit.event.Listener;



public class actionItem implements Listener {

    public Items items = new Items();

    public void inventaire(Player player, String cmd){
        if(cmd.equalsIgnoreCase("/party leave") || cmd.equalsIgnoreCase("/party disband")){
            player.getInventory().clear();

            items.baseinv(player, true);
            player.updateInventory();

        }
        if(cmd.equalsIgnoreCase("/party create")){

            player.getInventory().clear();
            items.baseinv(player, false);
            items.ffa(player);
            items.team(player);
            player.updateInventory();

        }
        if(cmd.equalsIgnoreCase("/party accept")){
            player.getInventory().clear();
            items.baseinv(player, false);
            items.leave(player);
            player.updateInventory();

        }
    }
}

