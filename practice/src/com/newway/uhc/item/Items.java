package com.newway.uhc.item;



//import com.newway.uhc.commands.party;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.ArrayList;
import java.util.Arrays;

public class Items implements Listener {


    public Inventory inv;
    public ArrayList<ItemStack> inve = new ArrayList<ItemStack>();

    public ItemStack addNameandEffect(ItemStack item, String nom, String description, boolean enchant){

        ItemMeta customParty = item.getItemMeta();
        customParty.setDisplayName(nom);
        customParty.setLore(Arrays.asList(description));

        if(enchant){
            customParty.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
            customParty.addItemFlags(ItemFlag.HIDE_ENCHANTS);

        }
        item.setItemMeta(customParty);
        return item;
    }
    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        player.getInventory().clear();
        if(player.isOp()){
            ItemStack customnether = new ItemStack(Material.NETHER_STAR);
            addNameandEffect(customnether, "§8Menu", "§aClick droit pour acceder au menu", true);
            player.getInventory().setItem(4, customnether);
            player.updateInventory();
        }
        player.getInventory().setItem(6, partyItem(player));
    }
    public ItemStack partyItem(Player player){

        ItemStack party = new ItemStack(Material.NAME_TAG);
        ItemMeta customParty = party.getItemMeta();
        customParty.setDisplayName("§5Créer une party");
        customParty.setLore(Arrays.asList("§3Clique Droit pour créer une party"));
        party.setItemMeta(customParty);
        customParty.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
        customParty.addItemFlags(ItemFlag.HIDE_ENCHANTS);



        return party;
    }

    public void ffa(Player player){
        ItemStack customffa = new ItemStack(Material.IRON_AXE);
        addNameandEffect(customffa, "§aFFA", "§cClick droit pour lancer une party FFA", true);
        player.getInventory().setItem(7, customffa);
        player.updateInventory();
    }

    public void team(Player player){
        ItemStack customteam = new ItemStack(Material.GOLD_AXE);
        addNameandEffect(customteam, "§aTeam", "§cClick droit pour lancer une party en Team", true);
        player.getInventory().setItem(8, customteam);
        player.updateInventory();
    }

    public void leave(Player player){
        ItemStack customleave = new ItemStack(Material.RED_SANDSTONE);
        addNameandEffect(customleave, "§4Leave", "§cClick droit pour quitter la party", false);
        player.getInventory().setItem(8, customleave);
        player.updateInventory();

    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Action action = event.getAction();
        Player player = event.getPlayer();
        ItemStack it = event.getItem();
        if(it ==  null ||it.getType() == null){
            return;
        }else{
            if(it.getType() == Material.NETHER_STAR && it.hasItemMeta() && ( action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR ) ){
                inv = Bukkit.createInventory(null, 54, "§8Menu");
                inv.setItem(11, getItem(Material.DIAMOND_SWORD, "§aPractice"));
                player.openInventory(inv);
                player.updateInventory();
            }
            if(it.getType() == Material.NAME_TAG && it.hasItemMeta() && ( action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR ) ){
                player.chat("/party create");
            }
            if(it.getType() == Material.IRON_AXE && it.hasItemMeta() && ( action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR )){
                Inventory inv2 = Bukkit.createInventory(null, 54, "§8MenuFFA");
                inv2.setItem(11, getItem(Material.GOLDEN_APPLE, "§aBuildUhc"));
                player.openInventory(inv2);
                player.updateInventory();
            }

            if(it.getType() == Material.RED_SANDSTONE && it.hasItemMeta() && ( action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR )){
                player.chat("/party leave");
            }

            if(it.getType() == Material.GOLD_AXE && it.hasItemMeta() && ( action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR )){
                Inventory inv2 = Bukkit.createInventory(null, 54, "§8MenuTeam");
                inv2.setItem(11, getItem(Material.GOLDEN_APPLE, "§aBuildUhc"));
                player.openInventory(inv2);
                player.updateInventory();
            }
        }




    }


    public ItemStack getItem(Material material, String name){
        ItemStack it = new ItemStack(material);
        ItemMeta itm = it.getItemMeta();
        itm.setDisplayName(name);
        it.setItemMeta(itm);
        return it;

    }
    public void baseinv(Player player, boolean create){
        player.getInventory().clear();
        if(player.isOp()){
            ItemStack customnether = new ItemStack(Material.NETHER_STAR);
            addNameandEffect(customnether, "§8Menu", "§aClick droit pour acceder au menu", true);
            player.getInventory().setItem(4, customnether);
            player.updateInventory();
        }
        if(create){
            player.getInventory().setItem(6, partyItem(player));
        }else{
            player.getInventory().remove(partyItem(player));
        }
    }



}
