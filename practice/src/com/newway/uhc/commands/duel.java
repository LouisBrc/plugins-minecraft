package com.newway.uhc.commands;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;


public class duel implements CommandExecutor {

    public Map<Player, Player> players = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player player = (Player) sender;
        if (player instanceof Player) {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("accept")) {
                    if (players.containsKey(player)) {
                        player.sendMessage("§cDebut du duel");

                        Player first = players.get(player);
                        first.sendMessage("§cLe duel se lance");
                        player.teleport(new Location(Bukkit.getWorld("world"), -7, 72, 263));
                        first.teleport(new Location(Bukkit.getWorld("world"), -7, 72, 255));
                        players.remove(player);
                    } else {
                        player.sendMessage("§4Vous n'avez pas de demandes de duel en cours");
                    }
                } else if (args[0].equalsIgnoreCase("deny")) {
                    if (players.containsKey(player)) {
                        player.sendMessage("§cDuel refusé");
                        Player first = players.get(player);
                        first.sendMessage("§cLe joueur §a" + player.getName() + "§c refusé le duel");
                        players.remove(player);
                    } else {
                        player.sendMessage("Vous n'avez pas de demandes de duel en cours");

                    }
                } else {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null && target != player) {
                        if (players.containsKey(target)) {
                            player.sendMessage("§a" + target.getName() + " §ca déjà une demande de duel en cours");
                            return false;
                        }else{
                            player.sendMessage("§cVous avez envoyé une demande de duel à §a" + args[0]);
                            players.put(target, player);
                            target.sendMessage("§a" + player.getName() + " §cvous a envoyé une demande de duel");
                        }

                        player.sendMessage("§cVous avez envoyé une demande de duel à §a" + args[0]);
                        players.put(target, player);
                        target.sendMessage("§a" + player.getName() + " §cvous a envoyé une demande de duel");
                    } else {
                        player.sendMessage("§cLe joueur §a" + args[0] + " §cn'existe pas, ou alors c'est vous");
                    }
                }
            }else{
                player.sendMessage("§4/duel <player> pour lancer le duelml");
            }

            return false;
        }
        return false;
    }

}
