package com.newway.uhc.commands;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_8_R3.ServerPing;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EnterCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 1) {
                TextComponent msg = new TextComponent("§4Mot de passe entré");
                msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§aRentrer le mot de passe").create()));

                player.spigot().sendMessage(msg);
            } else {
                player.sendMessage(("§3/" + label + " §3{Mot de passe(que un seul argument)}"));
                return false;

            }
        } else {
            if (args.length == 1) {
                TextComponent msg = new TextComponent("§4Mot de passe entré");
                msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("&aRentrer le mot de passe").create()));
                sender.sendMessage(String.valueOf(msg));

            } else {
                sender.sendMessage("/" + label + " {Mot de passe(que un seul argument)}");
                return false;

            }
        }
        return false;
    }

}
