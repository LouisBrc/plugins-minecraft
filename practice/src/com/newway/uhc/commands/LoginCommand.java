package com.newway.uhc.commands;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class LoginCommand implements CommandExecutor, Listener {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(args.length >= 1){
                String lien = "https://twitter.com/" + args[0];
                TextComponent msg = new TextComponent("§4Lien de mon twitter");
                msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("&aClick for open").create()));
                msg.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, lien));

                player.spigot().sendMessage(msg);
            }else{
                player.sendMessage(label + " {Twitter}");
                return false;

            }
        }else {
            if(args.length >= 1){
                String lien = "https://twitter.com/" + args[0];
                sender.sendMessage(lien);


            }else{
                sender.sendMessage(label + "{Twitter}");
                return false;

            }
        }

        return false;
    }
}
