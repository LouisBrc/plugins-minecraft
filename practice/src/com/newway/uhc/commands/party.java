package com.newway.uhc.commands;

import com.newway.uhc.item.*;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import org.bukkit.entity.Player;
import com.newway.uhc.partylist.*;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class party implements CommandExecutor {


    public Map<Player, Player> players = new HashMap<>();


    actionItem itemlist = new actionItem();

    public menu newmenu = new menu();

    public playerlist playerlist = new playerlist();

    @Override
    public boolean onCommand(CommandSender sender, Command smd, String label, String[] args) {



        Player player = (Player) sender;
        if (sender instanceof Player){

            if (args.length == 0 || args.length > 2) {
                player.sendMessage("---------------");
                player.sendMessage("Voici la list des commandes : ");
                player.sendMessage("/party invite [pseudo]");
                player.sendMessage("/party create");
                player.sendMessage("/party disband");
                player.sendMessage("/party leave");
                player.sendMessage("/party kick [pseudo]");
                player.sendMessage("---------------");
                return false;
            } else if (args.length == 1){
                if (args[0].equals("create")){
                    if(newmenu.isin(player) == false){
                        itemlist.inventaire(player, "/party create");
                        player.sendMessage("Party crée.");

                        ArrayList party = new ArrayList<Player>();
                        party.add(player);
                        playerlist.playerlists.add(party);
                    }else{
                        player.sendMessage("La party est déjà crée");
                    }

                } else if (args[0].equals("disband")){
                    if(newmenu.isin(player)){
                        if(newmenu.place(player)[1] == 0){


                            newmenu.sendAll(player, "La party a été disband");
                            int[] tab = newmenu.place(player);
                            playerlist.playerlists.remove(tab[0]);

                        }else{
                            player.sendMessage("Tu n'es pas le chef de la party ! ");
                        }
                    }else{
                        player.sendMessage("Tu n'es pas dans une party");
                    }

                } else if (args[0].equals("leave")){
                    if(newmenu.isin(player) == true){
                        itemlist.inventaire(player, "/party leave");
                        int[] tab = newmenu.place(player);
                        player.sendMessage("Party quitée");

                        if(playerlist.playerlists.get(tab[0]).get(0) == player && playerlist.playerlists.get(tab[0]).size() > 1){
                            newmenu.sendAll(player , "Le nouveau leader de la party est : " + playerlist.playerlists.get(tab[0]).get(1).getName());
                        }
                        playerlist.playerlists.get(tab[0]).remove(tab[1]);
                        newmenu.sendAll(player, "Le joueur " + player.getName().toString() + " a quitté la party");
                        players.remove(player);
                    }

                }else if (args[0].equals("accept")) {
                    if(players.containsKey(player)){
                        if(!newmenu.isin(player)){
                            itemlist.inventaire(player, "/party accept");
                            player.sendMessage("Vous avez rejoint la partie");
                            Player first = players.get(player);
                            first.sendMessage("Le joueur §a" + player.getName() + " a rejoint la party ! ");
                            playerlist.playerlists.get(newmenu.place(first)[0]).add(player);
                            newmenu.telepor(player);
                        }else{
                            player.sendMessage("Vous êtes déjà dans une party ! ");
                        }

                    }else{
                        player.sendMessage("Personne t'a invité t bête ou quoi");
                    }

                } else if (args[0].equals("deny")) {
                    if(players.containsKey(player)){
                        if(!newmenu.isin(player)){
                            player.sendMessage("Invitation refusée");
                            Player first = players.get(player);
                            first.sendMessage("Le joueur §a" + first.getName() + " a refusé l'invitation. ");
                        }else{
                            player.sendMessage("Celà ne change rien, vous êtes déjà dans une party ");
                        }

                    }else{
                        player.sendMessage("Personne t'a invité t bête ou quoi");
                    }
                } else {
                    player.sendMessage("La commande n'existe pas");
                    return false;
                }
            } else if (args.length == 2) {
                Player target = Bukkit.getPlayer(args[1]);
                if (target != null && target != player) {
                    if(newmenu.isin(player) == true){
                        if (args[0].equals("invite")) {
                            if (players.containsKey(target)) {
                                player.sendMessage("§a" + target.getName() + " §ca déjà une invitation en cours");
                            }
                            if(newmenu.isin(target)){
                                player.sendMessage(args[1] +  " est déjà dans une party");
                            }
                            else {
                                player.sendMessage("§cVous avez envoyé une invitation à §a" + args[1]);
                                players.put(target, player);
                                target.sendMessage("§a" + player.getName() + " §cvous a envoyé une invitation à sa party");
                            }
                        } else if (args[0].equals("kick")) {
                            if (playerlist.playerlists.contains(target)) {
                                player.sendMessage("Joueur " + args[1] + " exclu de la Party");
                                playerlist.playerlists.remove(target);
                            } else {
                                player.sendMessage("Le joueur §c" + args[1] + " n'est pas dans la partie");
                            }


                        } else {
                            player.sendMessage("La commande n'existe pas");
                        }

                    }else{
                        player.sendMessage("Tu n'es pas dans une party ! ");
                    }

                } else {
                    player.sendMessage("Le joueur " + args[1] + " n'existe pas");
                }
            }
            return false;

        }
        return false;
    }





}



