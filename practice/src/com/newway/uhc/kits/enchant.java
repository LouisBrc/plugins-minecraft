package com.newway.uhc.kits;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class enchant implements Listener {

    public ItemStack addEnchant(ItemStack item, ArrayList<Enchantment> ench, ArrayList<Integer> levels, Player player){

        ItemMeta itemme = item.getItemMeta();
        //System.out.println(levels.get(0));
        int i = 0;

        while (ench.size() > i){
            
            itemme.addEnchant(ench.get(i), levels.get(i), true);
            i++;
        }
        item.setItemMeta(itemme);
        return item;
    }
}
