package com.newway.uhc.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class builduhc implements Listener {
    public ArrayList<ItemStack> bu = new ArrayList<>();
    public static ArrayList<Enchantment> en = new ArrayList<>();
    public static ArrayList<Integer> niveau = new ArrayList<>();
    public inventory inv = new inventory();

    public enchant enchte = new enchant();
    public void addbu(Player player){
        player.updateInventory();
        bu.clear();
        ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
        bu.add(enchantbu(sword, player));
        clear();
        ItemStack gaps = new ItemStack(Material.GOLDEN_APPLE, 6);
        bu.add(gaps);
        ItemStack blocks = new ItemStack(Material.COBBLESTONE, 64);
        bu.add(blocks);
        ItemStack water = new ItemStack(Material.WATER_BUCKET, 1);
        bu.add(water);
        ItemStack lava = new ItemStack(Material.LAVA_BUCKET,1);
        bu.add(lava);
        ItemStack food = new ItemStack(Material.COOKED_BEEF,32);
        bu.add(food);
        ItemStack rod = new ItemStack(Material.FISHING_ROD,1);
        bu.add(rod);
        ItemStack bow = new ItemStack(Material.BOW,1);
        bu.add(bow);
        ItemStack pickaxe = new ItemStack(Material.DIAMOND_PICKAXE,1);
        bu.add(pickaxe);
        player.updateInventory();
        inv.without(player, bu);
    }

    public ItemStack enchantbu(ItemStack item, Player player){

        if(item.getType() == Material.DIAMOND_SWORD){

            en.add(Enchantment.DAMAGE_ALL);
            en.add(Enchantment.DURABILITY);
            niveau.add(3);
            niveau.add(7);
        }
        return enchte.addEnchant(item, en, niveau, player);
    }

    public void clear(){
        en.clear();
        niveau.clear();
    }


}
