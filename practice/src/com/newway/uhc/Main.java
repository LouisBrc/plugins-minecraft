package com.newway.uhc;

import com.newway.uhc.commands.*;
import com.newway.uhc.item.Items;
import com.newway.uhc.item.actionItem;
import com.newway.uhc.item.menu;
import com.newway.uhc.kits.builduhc;
import com.newway.uhc.kits.enchant;
import com.newway.uhc.kits.inventory;
import com.newway.uhc.partylist.ffa;
import com.newway.uhc.partylist.gamemodes;
import com.newway.uhc.partylist.playerlist;
import com.newway.uhc.partylist.team;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new enchant(), this);
        getServer().getPluginManager().registerEvents(new builduhc(), this);
        getServer().getPluginManager().registerEvents(new inventory(), this);
        getServer().getPluginManager().registerEvents(new ffa(), this);
        getServer().getPluginManager().registerEvents(new ffa(), this);
        getServer().getPluginManager().registerEvents(new team(), this);
        getServer().getPluginManager().registerEvents(new gamemodes(), this);
        getServer().getPluginManager().registerEvents(new menu(), this);
        getServer().getPluginManager().registerEvents(new playerlist(), this);
        getServer().getPluginManager().registerEvents(new Items(), this);
        getServer().getPluginManager().registerEvents(new actionItem(), this);
        Bukkit.getPluginManager().registerEvents(this, this);
        getCommand("duel").setExecutor(new duel());
        getCommand("log").setExecutor(new LoginCommand());
        getCommand("login").setExecutor(new EnterCommand());
        getCommand("spawn").setExecutor(new spawn());
        getCommand("party").setExecutor( new party());
        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        player.sendMessage("§eBienvenue sur le serveur ! ");
    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event){
        Player player = event.getPlayer();
        player.chat("/party leave");
    }









}